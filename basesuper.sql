-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.1.9-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win64
-- HeidiSQL Versión:             9.1.0.4867
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Volcando estructura para tabla poo.producto_tienda
CREATE TABLE IF NOT EXISTS `producto_tienda` (
  `nombre` varchar(70) DEFAULT NULL,
  `descripcion` varchar(30) DEFAULT NULL,
  `precio` decimal(8,2) DEFAULT NULL,
  `imagenes` varchar(70) DEFAULT NULL,
  `id_producto` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla poo.producto_tienda: ~17 rows (aproximadamente)
DELETE FROM `producto_tienda`;
/*!40000 ALTER TABLE `producto_tienda` DISABLE KEYS */;
INSERT INTO `producto_tienda` (`nombre`, `descripcion`, `precio`, `imagenes`, `id_producto`) VALUES
	('burger', 'de carne', 6.50, 'assets/imagenes/anuncio1.jpg', 1),
	('hotdog', 'con ketchup', 2.50, 'assets/imagenes/hotdog.jpg', 2),
	('hotdog', 'con mostaza', 2.60, 'assets/imagenes/hotdog.jpg', 3),
	('muñeco de seda', 'artesanal', 20.00, 'assets/imagenes/seda.jpg', 4),
	('polo blanco', 'marca adidas', 80.00, 'assets/imagenes/adidaspolo.jpg', 5),
	('taza', 'de barro', 10.00, 'assets/imagenes/tazab.jpg', 6),
	('teclado', 'marca logitech', 60.00, 'assets/imagenes/logitech.jpg', 7),
	('regla T', 'de arquitecto', 30.00, 'assets/imagenes/regla.jpg', 8),
	('cuchillo', 'generico', 14.00, 'assets/imagenes/cuchi.jpg', 9),
	('Pack de lapices', 'para arte', 16.00, 'assets/imagenes/lapices.jpg', 10),
	('Bicicleta montañera', 'incluye timbre', 150.00, 'assets/imagenes/bici.jpg', 11),
	('pantalla de plasma LG 24', 'imagenes HD', 45.00, 'assets/imagenes/plasma.jpg', 12),
	('pack de cuchillos de cocina', 'Acero inoxidable', 30.00, '../../assets/imagenes/packc.png', 13),
	('Audifonos', 'marca Skullcandy', 10.00, 'assets/imagenes/Skullcandy.jpg', 14),
	('Gorro', 'de lana', 20.00, 'assets/imagenes/gorro.jpg', 15),
	('Laminas de oro', 'comestible', 22.86, 'assets/imagenes/lamina.jpg', 16),
	('Lupa', 'aumento 20x', 13.00, 'assets/imagenes/lupa.jpg', 17);
/*!40000 ALTER TABLE `producto_tienda` ENABLE KEYS */;


-- Volcando estructura para tabla poo.usuario_tienda
CREATE TABLE IF NOT EXISTS `usuario_tienda` (
  `id` varchar(8) DEFAULT NULL,
  `nombre` varchar(30) DEFAULT NULL,
  `correo_electronico` varchar(20) DEFAULT NULL,
  `DNI` varchar(8) DEFAULT NULL,
  `contrasenia` varchar(15) DEFAULT NULL,
  `numero_celular` varchar(9) DEFAULT NULL,
  `ubicacion_usuario` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla poo.usuario_tienda: ~2 rows (aproximadamente)
DELETE FROM `usuario_tienda`;
/*!40000 ALTER TABLE `usuario_tienda` DISABLE KEYS */;
INSERT INTO `usuario_tienda` (`id`, `nombre`, `correo_electronico`, `DNI`, `contrasenia`, `numero_celular`, `ubicacion_usuario`) VALUES
	('1', 'gian', 'gian@gmail.com', '123', 'gian', '963206612', 'Lima'),
	('2', 'diego', 'diego@gmail.com', '001', 'francia', '111111111', 'Lima'),
	('', '', 'gian@gmail.com', '', '*****', '', ''),
	('', '', 'gian@gmail.com', '', '*****', '', ''),
	('', '', 'gian@gmail.com', '', '*****', '', ''),
	('', '', 'ggg', '', '34124', '', '');
/*!40000 ALTER TABLE `usuario_tienda` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
