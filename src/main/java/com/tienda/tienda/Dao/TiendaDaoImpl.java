package com.tienda.tienda.Dao;

import com.tienda.tienda.Dto.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

@Repository
public class TiendaDaoImpl implements TiendaDao{
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public Usuario guardarUsuario(Usuario usuario){

        String SQL="INSERT INTO usuario_tienda(id,nombre,correo_electronico,DNI,contrasenia,numero_celular,ubicacion_usuario)" +
                " VALUES (?,?,?,?,?,?,?)";
        try{
            Connection conexion= jdbcTemplate.getDataSource().getConnection();
            PreparedStatement ps= conexion.prepareStatement(SQL);
            ps.setString(1,usuario.getId());
            ps.setString(2,usuario.getNombre());
            ps.setString(3,usuario.getCorreo_electronico());
            ps.setString(4,usuario.getDni());
            ps.setString(5,usuario.getContrasenia());
            ps.setString(6,usuario.getNumero_celular());
            ps.setString(7,usuario.getUbicacion_usuario());
            ps.execute();
            conexion.close();
        }catch(SQLException throwables){
            throwables.printStackTrace();
        }
        return usuario;
    }

    @Override
    public ListaProductos obtenerListaProductos(Producto producto){
        ListaProductos retorno = new ListaProductos();
        retorno.setLista(new ArrayList<Producto>());
        String SQL = "SELECT nombre,descripcion,precio,id_producto " +
                "FROM producto_tienda " +
                "WHERE nombre LIKE ? ";

        try {
            Connection con = jdbcTemplate.getDataSource().getConnection();
            PreparedStatement ps = con.prepareStatement(SQL);
            ps.setString(1,"%"+producto.getNombre()+"%");
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                Producto produc = new Producto();
                produc.setNombre(rs.getString("nombre"));
                produc.setDescripcion(rs.getString("descripcion"));
                produc.setPrecio(rs.getDouble("precio"));
                produc.setId_producto(rs.getInt("id_producto"));
                retorno.getLista().add(produc);
            }
            con.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return retorno;
    }

    @Override
    public ListaProductos buscarProductoNombre(Producto producto){
        ListaProductos retorno = new ListaProductos();
        retorno.setLista(new ArrayList<Producto>());
        String SQL = "SELECT * " +
                "FROM producto_tienda " +
                "WHERE nombre LIKE ?";
        try{
            Connection con = jdbcTemplate.getDataSource().getConnection();
            PreparedStatement ps = con.prepareStatement(SQL);
            ps.setString(1,"%"+producto.getNombre()+"%");
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                Producto produc = new Producto();
                produc.setNombre(rs.getString("nombre"));
                produc.setDescripcion(rs.getString("descripcion"));
                produc.setPrecio(rs.getDouble("precio"));
                produc.setImagenUrl(rs.getString("imagenes"));
                produc.setId_producto(rs.getInt("id_producto"));
                retorno.getLista().add(produc);
            }
            rs.close();
            ps.close();
            con.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return retorno;
    }

    @Override
    public Usuario devolverUsuario(Usuario usuario){

        String SQL="select id,nombre,correo_electronico,DNI,contrasenia,numero_celular,ubicacion_usuario from usuario_tienda " +
                " where correo_electronico = ? and contrasenia = ?";
        Usuario usua= null;
        try{
            Connection conexion= jdbcTemplate.getDataSource().getConnection();
            PreparedStatement ps = conexion.prepareStatement(SQL);
            ps.setString(1,usuario.getCorreo_electronico());
            ps.setString(2,usuario.getContrasenia());
            ResultSet resultados= ps.executeQuery();
            while(resultados.next()){
                usua= new Usuario();
                usua.setId(resultados.getString("id"));
                usua.setNombre(resultados.getString("nombre"));
                usua.setCorreo_electronico(resultados.getString("correo_electronico"));
                usua.setDni(resultados.getString("dni"));
                usua.setContrasenia(resultados.getString("contrasenia"));
                usua.setNumero_celular(resultados.getString("numero_celular"));
                usua.setUbicacion_usuario(resultados.getString("ubicacion_usuario"));
            }
            resultados.close();
            ps.close();
            conexion.close();
        }catch(SQLException throwables){
            throwables.printStackTrace();
        }
        return usua;
    }
    @Override
    public Usuario recuperarContra(Usuario usuario){
        String SQL = "SELECT contrasenia FROM usuario_tienda " +
                "WHERE correo_electronico = ? ";
        Usuario usua= null;
        try{
            Connection conexion= jdbcTemplate.getDataSource().getConnection();
            PreparedStatement ps = conexion.prepareStatement(SQL);
            ps.setString(1,usuario.getCorreo_electronico());
            ResultSet resultados= ps.executeQuery();
            while(resultados.next()){
                usua= new Usuario();
                usua.setContrasenia(resultados.getString("contrasenia"));
            }
            resultados.close();
            ps.close();
            conexion.close();
    }catch(SQLException throwables){
        throwables.printStackTrace();
    }
        return usua;
    }

    @Override
    public ListaProductosTransaccionado obtenerListaProductosTransaccionado(ProductoTransaccionado productoTransaccionado){
        ListaProductosTransaccionado retorno = new ListaProductosTransaccionado();
        retorno.setLista(new ArrayList<ProductoTransaccionado>());
        String SQL = "SELECT nombre,descripcion,precio,id_producto,fecha_compra " +
                "FROM producto_adquirido " +
                "WHERE nombre LIKE ? ";

        try {
            Connection con = jdbcTemplate.getDataSource().getConnection();
            PreparedStatement ps = con.prepareStatement(SQL);
            ps.setString(1,"%"+productoTransaccionado.getNombre()+"%");
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                ProductoTransaccionado produc = new ProductoTransaccionado();
                produc.setNombre(rs.getString("nombre"));
                produc.setDescripcion(rs.getString("descripcion"));
                produc.setPrecio(rs.getDouble("precio"));
                produc.setId_producto(rs.getInt("id_producto"));
                produc.setFecha_venta(rs.getString("fecha_compra"));
                retorno.getLista().add(produc);
            }
            con.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return retorno;
    }

    @Override
    public ListaProductos ordenarDatosCosto(){
        ListaProductos retorno = new ListaProductos();
        retorno.setLista(new ArrayList<Producto>());
        String SQL = "SELECT nombre,descripcion,precio,id_producto from producto_tienda ORDER BY 3 DESC;";

        try {
            Connection con = jdbcTemplate.getDataSource().getConnection();
            PreparedStatement ps = con.prepareStatement(SQL);
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                Producto produc = new Producto();
                produc.setNombre(rs.getString("nombre"));
                produc.setDescripcion(rs.getString("descripcion"));
                produc.setPrecio(rs.getDouble("precio"));
                produc.setId_producto(rs.getInt("id_producto"));
                retorno.getLista().add(produc);
            }
            con.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return retorno;
    }

    @Override
    public ListaProductosTransaccionado ordenarDatosFecha(){
        ListaProductosTransaccionado retorno = new ListaProductosTransaccionado();
        retorno.setLista(new ArrayList<ProductoTransaccionado>());
        String SQL = "SELECT nombre,descripcion,precio,id_producto,fecha_venta from producto_vendido ORDER BY 5 ASC ";
        try {
            Connection con = jdbcTemplate.getDataSource().getConnection();
            PreparedStatement ps = con.prepareStatement(SQL);
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                ProductoTransaccionado produc = new ProductoTransaccionado();
                produc.setNombre(rs.getString("nombre"));
                produc.setDescripcion(rs.getString("descripcion"));
                produc.setPrecio(rs.getDouble("precio"));
                produc.setId_producto(rs.getInt("id_producto"));
                produc.setFecha_venta(rs.getString("fecha_venta"));
                retorno.getLista().add(produc);
            }
            con.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return retorno;
    }
}
