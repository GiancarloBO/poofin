package com.tienda.tienda.Dao;

import com.tienda.tienda.Dto.*;

public interface TiendaDao {
    public Usuario guardarUsuario(Usuario usuario);
    public ListaProductos obtenerListaProductos(Producto producto);
    public Usuario devolverUsuario(Usuario usuario);
    public ListaProductos buscarProductoNombre(Producto producto);
    public Usuario recuperarContra(Usuario usuario);
    public ListaProductos ordenarDatosCosto();
    public ListaProductosTransaccionado ordenarDatosFecha();
    public ListaProductosTransaccionado obtenerListaProductosTransaccionado(ProductoTransaccionado productoTransaccionado);
}
