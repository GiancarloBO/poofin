package com.tienda.tienda.Service;

import com.tienda.tienda.Dao.TiendaDao;
import com.tienda.tienda.Dto.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class TiendaServiceImpl implements TiendaService{
    @Autowired
    private TiendaDao tiendaDao;

    public Usuario guardarUsuario(Usuario usuario){
        return tiendaDao.guardarUsuario(usuario);
    }

    public ListaProductos obtenerListaProductos(Producto producto){
        return tiendaDao.obtenerListaProductos(producto);
    }

    public Usuario devolverUsuario(Usuario usuario){
        return tiendaDao.devolverUsuario(usuario);
    }
    public Usuario recuperarContra(Usuario usuario){
        return tiendaDao.recuperarContra(usuario);
    }

    public ListaProductosTransaccionado obtenerListaProductosTransaccionado(ProductoTransaccionado productoTransaccionado){
        return tiendaDao.obtenerListaProductosTransaccionado(productoTransaccionado);
    }

    public ListaProductos ordenarDatosCosto(){
        return tiendaDao.ordenarDatosCosto();
    }
    public ListaProductosTransaccionado ordenarDatosFecha(){
        return tiendaDao.ordenarDatosFecha();
    }

    public ListaProductos buscarProductoNombre(Producto producto){
            return tiendaDao.buscarProductoNombre(producto);
    }

}
