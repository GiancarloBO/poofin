package com.tienda.tienda.Service;

import com.tienda.tienda.Dto.*;

public interface TiendaService {
    public Usuario guardarUsuario(Usuario usuario);
    public ListaProductos obtenerListaProductos(Producto producto);
    public Usuario devolverUsuario(Usuario usuario);
    public Usuario recuperarContra(Usuario usuario);
    public ListaProductos buscarProductoNombre(Producto producto);
    public ListaProductosTransaccionado ordenarDatosFecha();
    public ListaProductosTransaccionado obtenerListaProductosTransaccionado(ProductoTransaccionado productoTransaccionado);
    public ListaProductos ordenarDatosCosto();
}
