package com.tienda.tienda.Dto;
import lombok.Data;
import java.util.List;
@Data
public class ListaProductosTransaccionado {
    private List<ProductoTransaccionado> lista;
}
