package com.tienda.tienda.Dto;

import lombok.Data;

@Data
public class Usuario {
    String id;
    String nombre;
    String correo_electronico;
    String dni;
    String contrasenia;
    String numero_celular;
    String ubicacion_usuario;
}
