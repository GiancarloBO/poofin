package com.tienda.tienda.Dto;
import lombok.Data;
import java.util.List;
@Data
public class ListaProductos {
    private List<Producto> lista;
}
