package com.tienda.tienda.Dto;

import lombok.Data;

@Data
public class ProductoTransaccionado extends Producto{
    String fecha_venta;
}
