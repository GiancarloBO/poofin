package com.tienda.tienda.Dto;

import lombok.Data;

@Data
public class Producto {
    String nombre;
    String descripcion;
    Double precio;
    String imagenUrl;
    Integer id_producto;
}
