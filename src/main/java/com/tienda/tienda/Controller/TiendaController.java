package com.tienda.tienda.Controller;

import com.tienda.tienda.Dto.*;
import com.tienda.tienda.Service.TiendaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TiendaController {
    @Autowired
    private TiendaService tiendaService;

    @RequestMapping(value = "/CrearUsuario",
            method = RequestMethod.POST,
            produces = "application/json;charset=utf-8")
    public Usuario guardarUsuario(@RequestBody Usuario usuario){
        return tiendaService.guardarUsuario(usuario);
    }

    @RequestMapping(value = "/BuscarProducto",
            method = RequestMethod.POST,
            produces = "application/json;charset=utf-8")
    public ListaProductos obtenerListaProductos(@RequestBody Producto producto){
        return tiendaService.obtenerListaProductos(producto);
    }

    @RequestMapping(value = "/BuscarUsuario",
            method = RequestMethod.POST,
            produces = "application/json;charset=utf-8")
    public Usuario devolverUsuario(@RequestBody Usuario usuario){
        return tiendaService.devolverUsuario(usuario);
    }

    @RequestMapping(value = "/BuscarProductoTransaccionado",
            method = RequestMethod.POST,
            produces = "application/json;charset=utf-8") //RequestBody cuando tienes que meter dto
    public ListaProductosTransaccionado obtenerListaProductosTransaccionado(@RequestBody ProductoTransaccionado productoTransaccionado){
        return tiendaService.obtenerListaProductosTransaccionado(productoTransaccionado);
    };

    @RequestMapping(value = "/RecuperarContra",
            method = RequestMethod.POST,
            produces = "application/json;charset=utf-8")
    public Usuario recuperarContra(@RequestBody Usuario usuario){
        return tiendaService.recuperarContra(usuario);
    };

    @RequestMapping(value = "/OrdenarDatosCosto",
            method = RequestMethod.POST,
            produces = "application/json;charset=utf-8")
    public ListaProductos ordenarDatosCosto(){
        return tiendaService.ordenarDatosCosto();
    };

    @RequestMapping(value = "/OrdenarDatosFecha",
            method = RequestMethod.POST,
            produces = "application/json;charset=utf-8")
    public ListaProductosTransaccionado ordenarDatosFecha(){
        return tiendaService.ordenarDatosFecha();
    };

    @RequestMapping(value = "/BuscarProductoNombre",
            method = RequestMethod.POST,
            produces = "application/json;charset=utf-8")
    public ListaProductos buscarProductoNombre(@RequestBody Producto producto){
        return tiendaService.buscarProductoNombre(producto);
    }
}
